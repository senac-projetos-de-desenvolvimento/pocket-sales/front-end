import React from "react";
import { FiPlus } from "react-icons/fi";
import "../../css/Project.css";
const NewProjectTile = ({ showSideProjectForm }) => {
  return (
    <div className="project-tile-container" onClick={showSideProjectForm}>
      <div className="project-tile-box">
        <div className="new-project-tile-icon-container">
          <FiPlus className="new-project-tile-icon" />
        </div>
      </div>
      <div className="project-tile-name">Novo Projeto</div>
      <img className="man-2" src="/img/sign/project.png" alt="" />
    </div>
  );
};

export default NewProjectTile;
