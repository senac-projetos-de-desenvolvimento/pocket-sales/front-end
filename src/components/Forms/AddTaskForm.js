import React, { useState, useContext } from "react";
import "../../css/Task.css";
import { useForm } from "react-hook-form";
import apiServer from "../../config/apiServer";
import { Context as ProjectContext } from "../../context/store/ProjectStore";
import { Context as TaskContext } from "../../context/store/TaskStore";

//Form to add task from anywhere
const TaskForm = ({
  handleNewClose,
  clickClose,
  open,
  setTasklists,
  showSideTaskForm,
}) => {
  const { register, handleSubmit, errors, clearErrors } = useForm();
  const [projectError] = useState();
  const [assigneeError] = useState();
  const [taskName, setTaskName] = useState();
  const [dueDate, setDueDate] = useState();

  const [projectState] = useContext(ProjectContext);
  const [taskdispatch] = useContext(TaskContext);
  const [projectUsers, setProjectUsers] = useState([
    {
      id: "0",
      name: "Escolha o projeto primeiro",
    },
  ]);
  const [projectTaskLists, setProjectTaskLists] = useState([
    {
      id: "0",
      name: "Escolha o projeto primeiro",
    },
  ]);

  const handleNameChange = (e) => {
    setTaskName(e.target.value);
  };
  const handleDateChange = (e) => {
    setDueDate(e.target.value);
  };
  const getProjectUsers = async (event) => {
    var projectSelect = document.getElementById("project-select");
    var assigneeSelect = document.getElementById("assignee-select");
    var tasklistSelect = document.getElementById("tasklist-select");
    clearErrors(projectSelect.name);
    clearErrors(assigneeSelect.name);
    clearErrors(tasklistSelect.name);
    const res = await apiServer.get(`/project/${projectSelect.value}/team`);
    setProjectUsers(res.data.Users);
    getProjectTasklists();
  };

  const getProjectTasklists = async (event) => {
    const select = document.getElementById("project-select");
    const res = await apiServer.get(`/project/${select.value}/tasklists`);
    setProjectTaskLists(res.data);
  };

  //Probably need dispatch here to update the task page when task is created.
  const onSubmit = async ({
    name,
    projectId,
    assigneeId,
    due_date,
    tasklistId,
    completed,
    description,
  }) => {
    await apiServer.post(`/tasklist/${tasklistId}/task`, {
      name,
      projectId,
      assigneeId,
      due_date,
      completed,
      description,
    });

    const userId = localStorage.getItem("userId");
    const res = await apiServer.get(`/task/user/${userId}`);
    await taskdispatch({ type: "get_user_tasks", payload: res.data });

    if (setTasklists) {
      const taskResponse = await apiServer.get(
        `/project/${projectId}/tasklists`
      );

      setTasklists(taskResponse.data);
    }

    showSideTaskForm();
  };

  const renderedProjects = projectState.projects.map((project, i) => {
    return (
      <option key={i} id={project.id} value={project.id}>
        {project.name}
      </option>
    );
  });

  const renderedUsers = projectUsers.map((user, i) => {
    return (
      <option key={i} value={user.id}>
        {user.name}
      </option>
    );
  });

  const renderedTasklists = projectTaskLists.map((tasklist, i) => {
    return (
      <option key={i} value={tasklist.id}>
        {tasklist.name}
      </option>
    );
  });

  return (
    <>
      <form className="form-container" onSubmit={handleSubmit(onSubmit)}>
        <div className="form-top-container">
          <div className="form-section">
            <div className="label-container">
              <label className="form-label">Nome da tarefa</label>
            </div>
            <div className="input-container">
              <input
                name="name"
                type="text"
                placeholder={"Nome da tarefa"}
                className="form-input"
                ref={register({ required: true })}
                onChange={handleNameChange}
              ></input>
              {errors.name?.type === "required" && (
                <p className="error-message">
                  Por favor insira um nome de tarefa
                </p>
              )}
            </div>

            <div className="label-container">
              <label className="form-label">Projeto</label>
            </div>
            <div className="input-container">
              <select
                id="project-select"
                name="projectId"
                className="form-input"
                onChange={getProjectUsers}
                ref={register({ required: true })}
              >
                <option value={0}>{"Escolha o Projeto"}</option>
                {renderedProjects}
              </select>
              <p className="error-message">{projectError}</p>
              {errors.projectId?.type === "required" && (
                <p className="error-message">Por favor, escolha o Projeto</p>
              )}
            </div>
          </div>
          <div className="form-section">
            <div className="label-container">
              <label className="form-label">Data de Vencimento</label>
            </div>
            <div className="input-container">
              <input
                className="form-input"
                type="date"
                name="due_date"
                ref={register({ required: true })}
                onChange={handleDateChange}
              ></input>
              {errors.due_date?.type === "required" && (
                <p className="error-message">Escolha uma data de vencimento</p>
              )}
            </div>
            <div className="label-container">
              <label className="form-label">Cessionário</label>
            </div>
            <div className="input-container">
              <select
                id="assignee-select"
                name="assigneeId"
                className="form-input"
                ref={register({ required: true })}
              >
                {renderedUsers}
              </select>
              <p className="error-message">{assigneeError}</p>
              {errors.assigneeId?.type === "required" && (
                <p className="error-message">
                  Por favor, escolha um cessionário
                </p>
              )}
            </div>
          </div>
          <div className="form-section">
            <div className="label-container">
              <label className="form-label">Marca Concluída</label>
            </div>
            <div className="input-container">
              <input
                style={{
                  margin: "9px 0px 18px 40px",
                  width: "16px",
                  height: "16px",
                }}
                type="checkbox"
                name="completed"
                defaultChecked={false}
                ref={register}
              ></input>
            </div>

            <div className="label-container">
              <label className="form-label">Coluna</label>
            </div>
            <div className="input-container">
              <select
                id="tasklist-select"
                name="tasklistId"
                className="form-input"
                ref={register({
                  required: true,
                })}
              >
                {projectTaskLists.length === 0 ? (
                  <option>
                    Você precisa fazer uma coluna em seu projeto primeiro.
                  </option>
                ) : (
                  renderedTasklists
                )}
              </select>
              {errors.tasklistId?.type === "required" && (
                <p className="error-message">
                  Escolha uma coluna. Você pode precisar fazer uma coluna em seu
                  projeto antes de adicionar uma tarefa.
                </p>
              )}
            </div>
          </div>
        </div>
        <div className="form-description-container">
          <textarea
            name="description"
            type="text"
            placeholder={"Descrição da tarefa"}
            className="edit-task-description textarea"
            ref={register}
          ></textarea>
        </div>

        <div className="form-button-container">
          <button
            className="cancel-button"
            onClick={showSideTaskForm}
            color="primary"
          >
            Cancelar
          </button>
          <button
            className={
              taskName && dueDate
                ? "submit-button enabled"
                : "submit-button disabled"
            }
            disabled={taskName && dueDate ? false : true}
            type="submit"
          >
            Criar Tarefa
          </button>
        </div>
      </form>
    </>
  );
};

export default TaskForm;
