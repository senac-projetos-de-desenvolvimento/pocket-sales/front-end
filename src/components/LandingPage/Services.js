/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import "../../css/style.css";

export default function Services() {
  return (
    <section className="services__area pt-120 pb-110 p-relative">
      <div className="container">
        <div className="row">
          <div className="col-xxl-8 offset-xxl-2 col-xl-10 offset-xl-1">
            <div
              className="section__title-wrapper section__title-wrapper-3 text-center section-padding-3 mb-80 wow fadeInUp"
              data-wow-delay=".3s"
            >
              <span className="section__pre-title-img">
                <img src="/img/icon/title/services.png" alt="" />
              </span>
              <h2 className="section__title section__title-3">
                Por que usar o Organizanto? 
              </h2>
              <p>
                Organizanto te dá tudo que você
                precisa para ficar em sincronia, cumpra os prazos e alcance seus
                objetivos
              </p>
            </div>
          </div>
        </div>
        <div className="row">
          <div
            className="col-xxl-4 col-xl-4 col-lg-4 col-md-6 wow fadeInUp"
            data-wow-delay=".3s"
          >
            <div className="services__item-3 white-bg transition-3 mb-30 text-center">
              <div className="services__icon-3">
                <img src="/img/services/2.png" alt="" />
              </div>
              <div className="services__content-3">
                <h3 className="services__title-3">
                  <a>Equipe</a>
                </h3>
                <p>
                  Estabeleça equipes com outros colegas e trabalhe em conjunto
                  para realizar tarefas.{" "}
                </p>
              </div>
            </div>
          </div>
          <div
            className="col-xxl-4 col-xl-4 col-lg-4 col-md-6 wow fadeInUp"
            data-wow-delay=".5s"
          >
            <div className="services__item-3 white-bg transition-3 mb-30 text-center">
              <div className="services__icon-3">
                <img src="/img/services/1.png" alt="" />
              </div>
              <div className="services__content-3">
                <h3 className="services__title-3">
                  <a>Projetos</a>
                </h3>
                <p>
                  Crie vários projetos dentro de uma equipe, categorize as
                  tarefas com base em diferentes tipos de projetos.{" "}
                </p>
              </div>
            </div>
          </div>
          <div
            className="col-xxl-4 col-xl-4 col-lg-4 col-md-6 wow fadeInUp"
            data-wow-delay=".7s"
          >
            <div className="services__item-3 white-bg transition-3 mb-30 text-center">
              <div className="services__icon-3">
                <img src="/img/services/3.png" alt="" />
              </div>
              <div className="services__content-3">
                <h3 className="services__title-3">
                  <a>Praticidade</a>
                </h3>
                <p>
                  Gerencie por meio de listas de tarefas em projetos
                  individuais e marque quando concluido.{" "}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div
          className="row"
          style={{ paddingTop: "100px", paddingBottom: "100px" }}
        >
          <div className="col-xxl-12 wow fadeInUp" data-wow-delay=".9s">
            <div className="services__more text-center mt-30">
              <a
                href="/login"
                className="w-btn w-btn-grey w-btn-grey-2 w-btn-3 w-btn-6"
              >
                Login como convidado
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
