/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useContext, useState } from "react";
import { useForm } from "react-hook-form";
import AuthContext from "../../context/AuthContext";
import "../../css/LoginPage.css";
import apiServer from "../../config/apiServer";
const RegisterPage = () => {
  const { register, handleSubmit, errors } = useForm();
  const { setAuth, setEmail, setUserId, setUser } = useContext(AuthContext);
  const [errorMessage, setErrorMessage] = useState("");
  const [loading, setLoading] = useState(false);
  const onSubmit = async ({ name, email, password }) => {
    setLoading(true);
    try {
      const res = await apiServer.post("/register", { name, email, password });
      localStorage.setItem("onboard", res.data.token);
      localStorage.setItem("email", res.data.email);
      localStorage.setItem("userId", res.data.id);
      window.location.href = "/register/onboard";
      setErrorMessage("");
      setUser(res.data);
      setAuth(res.data.token);
      setEmail(res.data.email);
      setUserId(res.data.id);
    } catch (err) {
      setLoading(false);
      setErrorMessage("Algo deu errado com o registro");
    }
  };

  return (
    <section className="signup__area po-rel-z1 pt-100 pb-145">
      <div className="sign__shape">
        <img className="man-1" src="/img/sign/man-3.png" alt="" />
        <img className="man-2 man-22" src="/img/sign/man-2.png" alt="" />
        <img className="circle" src="/img/sign/circle.png" alt="" />
        <img className="zigzag" src="/img/sign/zigzag.png" alt="" />
        <img className="dot" src="/img/sign/dot.png" alt="" />
        <img className="bg" src="/img/sign/sign-up.png" alt="" />
        <img className="flower" src="/img/sign/flower.png" alt="" />
      </div>
      <div className="container">
        <div className="row">
          <div className="col-xxl-8 offset-xxl-2 col-xl-8 offset-xl-2">
            <div className="page__title-wrapper text-center mb-55">
              <h2 className="page__title-2">
                Bem-vindo ao <br /> Organizanto!
              </h2>
              <p>Em primeiro lugar, vamos configurar sua conta...</p>
            </div>
          </div>
          <div className="sign__new text-center mt-20">
            <p>
              <a href="/sign-up">
                <a>Voltar a tela inicial</a>
              </a>
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-xxl-6 offset-xxl-3 col-xl-6 offset-xl-3 col-lg-8 offset-lg-2">
            <div className="sign__wrapper white-bg">
              <div className="sign__header mb-35">
                <div className="sign__in text-center">
                  <a className="sign__social g-plus text-start mb-15">
                    <i></i>Faça seu registro
                  </a>
                  <p>e aproveite o organizanto</p>
                </div>
              </div>
              <div className="sign__form">
                <form
                  className="register-page--form"
                  onSubmit={handleSubmit(onSubmit)}
                >
                  <div style={{ display: "flex", flexDirection: "column" }}>
                    <label htmlFor="name">Nome completo</label>
                    <input
                      name="name"
                      placeholder="Ex: Ana Soares"
                      ref={register({ required: true })}
                    ></input>
                    {errors.name?.type === "required" && (
                      <p style={{ color: "red", margin: "1px" }}>
                        Por favor, preencha o nome completo
                      </p>
                    )}
                  </div>
                  <div>
                    <label htmlFor="email">Email</label>
                    <input
                      name="email"
                      type="email"
                      placeholder="meuemail@gmail.com"
                      ref={register({ required: true })}
                    ></input>
                    {errors.email?.type === "required" && (
                      <p style={{ color: "red", margin: "1px" }}>
                        Por favor, preencha um email
                      </p>
                    )}
                  </div>

                  <div>
                    <label htmlFor="password">Senha</label>
                    <input
                      name="password"
                      type="password"
                      placeholder="xxxxxxx"
                      ref={register({ required: true })}
                    ></input>
                    {errors.password?.type === "required" && (
                      <p style={{ color: "red", margin: "1px" }}>
                        Por favor, preencha uma senha
                      </p>
                    )}
                  </div>
                  <button type="submit">
                    {loading ? "Registering.." : "Register"}
                  </button>
                  {errorMessage ? (
                    <p style={{ color: "red", margin: "1px" }}>
                      {errorMessage}
                    </p>
                  ) : null}
                </form>
                <div className="login-container">
                  Já tem uma conta?{" "}
                  <a
                    style={{ textDecoration: "none", color: "blue" }}
                    href="/login"
                  >
                    Clique aqui para fazer login
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default RegisterPage;
