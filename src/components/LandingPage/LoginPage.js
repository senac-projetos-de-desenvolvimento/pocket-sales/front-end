/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
 //  eslint-disable-next-line
import Link from "next/link";
import "../../css/style.css";
import "../../css/LoginPage.css";
import LoginForm from "../Forms/LoginForm";

const LoginPage = () => {
  return (
    <section className="signup__area po-rel-z1 pt-100 pb-145">
      <div className="sign__shape">
        <img className="man-1" src="/img/sign/man-1.png" alt="" />
        <img className="man-2" src="/img/sign/man-2.png" alt="" />
        <img className="circle" src="/img/sign/circle.png" alt="" />
        <img className="zigzag" src="/img/sign/zigzag.png" alt="" />
        <img className="dot" src="/img/sign/dot.png" alt="" />
        <img className="bg" src="/img/sign/sign-up.png" alt="" />
      </div>
      <div className="container">
        <div className="row">
          <div className="col-xxl-8 offset-xxl-2 col-xl-8 offset-xl-2">
            <div className="page__title-wrapper text-center mb-55">
              <h2 className="page__title-2">
                Bem vindo <br /> de volta!
              </h2>
              <a href="/register">
                <p>
                  Se você ainda não tem conta, pode se <a>registrar aqui!</a>
                </p>
              </a>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-xxl-6 offset-xxl-3 col-xl-6 offset-xl-3 col-lg-8 offset-lg-2">
            <div className="sign__new text-center mt-20">
              <p>
                <a href="/sign-up">
                  <a>Voltar a tela inicial</a>
                </a>
              </p>
            </div>
            <div className="sign__wrapper white-bg">
              <div className="sign__header mb-35">
                <div className="sign__in text-center">
                  <a className="sign__social text-start mb-15">
                    <i></i>Faça seu login
                  </a>
                  <p>entre utilizando seu email e senha </p>
                </div>
              </div>
              <div className="sign__form">
                <LoginForm />
                <div className="sign__new text-center mt-20">
                  <p>
                    Novo aqui? <a href="/register">Faça seu registro</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default LoginPage;
