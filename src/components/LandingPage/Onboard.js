import React, { useContext, useState } from "react";
import AuthContext from "../../context/AuthContext";
import apiServer from "../../config/apiServer";
import { useForm } from "react-hook-form";
import "../../css/LoginPage.css";

const Onboard = (props) => {
  const { register, handleSubmit, errors } = useForm();
  const { setAuth } = useContext(AuthContext);

  const [errorMessage, setErrorMessage] = useState("");
  const onboard = async ({ teamName }) => {
    const email = localStorage.getItem("email");
    if (teamName) {
      try {
        const res = await apiServer.put("/register/onboard", {
          email,
          teamName,
        });
        //sets initial token
        localStorage.setItem("token", res.data.token);
        setErrorMessage("");
        //for Refresh
        setAuth(res.data.token);
      } catch (err) {
        setErrorMessage("Something went wrong");
      }
    }
  };

  return (
    <div className="onboard-page-container">
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          marginTop: "200px",
        }}
      >
        <div className="onboard-page-header">
          <h1
            style={{
              fontWeight: "500",
              marginBottom: "20px",
              marginTop: "1px",
              fontSize: "24px",
            }}
          >
            Em que equipe você trabalhará?
          </h1>
        </div>
        <form className="onboard-page--form" onSubmit={handleSubmit(onboard)}>
          <div style={{ display: "flex", flexDirection: "column" }}>
            <label htmlFor="teamName">Nome do Time</label>
            <input name="teamName" ref={register({ minLength: 2 })}></input>
            {errors.teamName?.type === "minLengh" && (
              <p style={{ color: "red", margin: "1px" }}>
                O nome da equipe deve ser maior que 1 personagem
              </p>
            )}
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <button
              style={{
                width: "150px",
              }}
              type="submit"
            >
              Continuar
            </button>
          </div>
          {errorMessage ? (
            <p style={{ color: "red", margin: "1px" }}>{errorMessage}</p>
          ) : null}
        </form>
      </div>
    </div>
  );
};

export default Onboard;
