/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
 //  eslint-disable-next-line
import Link from "next/link";
import "../../css/style.css";
import picture from "../../assets/Product-screenshot.png";

export default function Footer3({ className }) {
  return (
    <footer className="footer__area footer-bg-3 pt-120 p-relative fix">
      <div className="footer__shape">
        <img
          className="footer-circle-2 footer-2-circle-2"
          src="/img/icon/hero/home-3/2.svg"
          alt=""
        />
      </div>
      <div className="footer__top pb-65">
        <div className="container">
          <div className="row">
            <div
              className="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-6 wow fadeInUp"
              data-wow-delay=".3s"
            >
              <div className="footer__widget mb-50">
                <div className="footer__widget-title mb-25">
                  <div className="footer__logo">
                    <a href="/">
                      <img src={picture} width="30%" alt="logo" />
                    </a>{" "}
                  </div>
                </div>
              </div>
            </div>
            <div
              className="col-xxl-2 col-xl-2 col-lg-2 col-md-4 col-sm-6 wow fadeInUp"
              data-wow-delay=".5s"
            >
              <div className="footer__widget-title footer__widget-title-3 mb-25">
                <div className="footer__widget-content footer__widget-content-3">
                  <h3>Faça uma gestão tranquila com Organizanto!</h3>
                </div>
              </div>
            </div>

            <div
              className="col-xxl-2 col-xl-3 col-lg-3 col-md-4 col-sm-6 wow fadeInUp"
              data-wow-delay="1.2s"
            >
              <div className="footer__widget mb-50 float-md-end fix">
                <div className="footer__widget-title footer__widget-title-3 mb-25">
                  <h3>Entre em contato</h3>
                </div>
                <div className="footer__widget-content">
                  <div className="footer__social footer__social-3">
                    <ul>
                      <li>
                        <a href="https://github.com/Mkroning">
                          <img src="/img/icons/1.png" width={40}></img>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.linkedin.com/in/matheus-kr%C3%B6ning/">
                          <img src="/img/icons/2.png" width={35}></img>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="footer__bottom">
        <div className="container">
          <div className="footer__copyright footer__copyright-2">
            <div className="row">
              <div className="col-xxl-12 wow fadeInUp" data-wow-delay="1.5s">
                <div className="footer__copyright-wrapper footer__copyright-wrapper-3 text-center">
                  <p>
                    @ 2021 Todos direitos reservados por <a>Matheus Kröning</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
