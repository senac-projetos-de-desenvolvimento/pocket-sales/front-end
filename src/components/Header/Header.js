/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { useState, useEffect } from "react";
import "../../css/style.css";

export default function Header() {
  const [scroll, setScroll] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY > 100);
    });
  }, [scroll]);

  return (
    <>
      <header>
        <div
          id="header-sticky"
          className={`header__area header__transparent header__padding ${
            scroll ? "sticky" : ""
          }`}
        >
          <div className="container">
            <div className="row align-items-center">
              <div className="col-xxl-3 col-xl-3 col-lg-2 col-md-6 col-6 "></div>
              <div className="col-xxl-6 col-xl-6 col-lg-7 d-none d-lg-block">
                <div className="main-menu"></div>
              </div>
              <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-6">
                <div className="header__right text-end d-flex align-items-center justify-content-end">
                  <div className="header__right-btn d-none d-md-flex align-items-center">
                    <a
                      href="/login"
                      className="header__btn"
                      style={{ marginright: "10px" }}
                    >
                      Login
                    </a>
                    <a href="/register" className="w-btn">
                      Registrar
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    </>
  );
}
