export const environment = process.env.NODE_ENV || "development";
export const backendUrl =
  // "https://organizanto-332514.uc.r.appspot.com/";
  process.env.NODE_ENV === "production"
    ? `https://organizanto-332514.uc.r.appspot.com/`
    : "http://localhost:8000/";
